#!/bin/bash

if [ "$RUNFROMSHELL" != "" ];
then
    echo "Running from shell. Skipping apt-get install"
    return
fi

echo "apt-get update"
./.ci/retry.sh apt-get update || exit 1
echo "apt-get -y install aptitude"
./.ci/retry.sh apt-get -y install aptitude || exit 1

echo "aptitude update"
./.ci/retry.sh aptitude update || exit 1
echo "aptitude -y install $*"
./.ci/retry.sh aptitude -y install $* || exit 1
