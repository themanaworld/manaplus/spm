SRCURL=https://github.com/leethomason/tinyxml2.git

ENV_PATH="lib:include"
ENV_LD_LIBRARY_PATH="lib"
ENV_LDFLAGS_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_CPPFLAGS_PATH="include"

BUILD_TYPE="cmake"
