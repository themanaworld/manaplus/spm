SRCTYPE=hg
SRCURL=http://hg.libsdl.org/SDL_image/
DEFAULT_BRANCH="SDL-1.2"

ENV_LD_LIBRARY_PATH="lib"
ENV_PKG_CONFIG_PATH="lib/pkgconfig"
ENV_LDFLAGS_PATH="lib"
ENV_CPPFLAGS_PATH="include/SDL"

ENABLE_SAME_DIR_BUILD="true"

BUILD_TYPE="configure"
SRC_INIT_COMMAND="./autogen.sh"
CONFIGURE_FLAGS="--disable-jpg-shared --disable-tif-shared --disable-png-shared --disable-webp-shared"
